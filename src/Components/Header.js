import React from 'react';
import {
    View,
    TouchableOpacity,
    Image,
} from 'react-native';
import {BoldText} from '../Components/styledTexts';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../styles/colors';

export default Header = (props) => {
    return (
        <View style={{flex:1 }}>
          <View style={{ flex:1, flexDirection:'row', alignItems:'flex-end', justifyContent:'flex-end', backgroundColor: props.backgroundColor ? props.backgroundColor : colors.appColor}}>
          <View style={{ paddingVertical:5, flex: 0.5, alignItems:'center', justifyContent:'center'}}>
            {props.leftNavigation ? (
            <TouchableOpacity 
              onPress={() => {
                  props.leftNavigation.goBack(null);
              }}
            >
              <Image style={{height: 25, width: 25, marginLeft: 5}}
                      resizeMode='contain'
                      source={require('../assets/ic_arrow_back.png')}
              />
                {/* <Icon name="angle-left" size={40} color="#FFF" /> */}
            </TouchableOpacity>
            ) : (null)}
 
            </View>
            <View style={{ flex: 2, alignItems:'center', justifyContent:'center', paddingVertical:5 }}>
                <BoldText style={{color: '#FFF', fontSize: 20,}}>{props.value}</BoldText>
           </View>
          <View style={{flex: 0.5}}>
          {props.rightIcon ? (
            <TouchableOpacity 
             onPress={()=> {
               props.onPressRight()}}
            >
              <Image style={{marginBottom: 7, height: 25, width: 25, marginLeft: 5, tintColor: '#fff'}}
                      resizeMode='contain'
                      source={props.rightIcon}
              />
            </TouchableOpacity>
          ) : (null)}

          </View>
          </View>
        </View>
    )
}