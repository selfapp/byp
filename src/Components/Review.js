import React from 'react';
import {
    View,
    Image,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import colors from '../styles/colors';
import { BoldText, LightText } from '../Components/styledTexts';

export default Review = (props) => {
    return (
        <View style={{flex:1, padding: 5, flexDirection: 'row' }}>
            <View style={{ padding: 5}}>
                <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'gray'}}>
                    <Image
                        source = {props.image}
                        style={{ height: 50, width: 50, borderRadius: 25 }}
                    />
                </View>
            </View>
            <View style={{ marginLeft: 8, paddingTop: 5, flex: 1, marginRight: 3 }}>
                <BoldText style={{ fontSize: 18 }}>{props.name}</BoldText>
                <View style={{height: 25, flexDirection:'row', alignItems:'center', }}>
                    <StarRating
                        style={{ flex: 1 }}
                        disabled={true}
                        maxStars={5}
                        starSize={18}
                        rating={props.star}
                        emptyStar={'ios-star-outline'}
                        fullStar={'ios-star'}
                        halfStar={'ios-star-half'}
                        iconSet={'Ionicons'}
                        fullStarColor={colors.lightYellow}
                    />
                    <LightText style={{color: colors.gray04, marginLeft: 5}}>{`${props.star}/5.0`}</LightText>
                </View>
                <View style={{marginTop: 15}}>
                <LightText style={{ textAlign: 'justify', lineHeight: 20}}>{props.comment}</LightText>
                <LightText style={{ marginTop: 10, textAlign: 'right'}}>{props.time}</LightText>
                </View>
            </View>
        </View>
    )
}