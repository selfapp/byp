import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Splash from '../Screens/Splash';
import InviteFriends from '../Screens/InviteFriends';

import Home from '../Screens/Home';
import List from '../Screens/List';
import ShopDetail from '../Screens/ShopDetail';
import ShowReviews from '../Screens/ShowReviews';

// SignUp
import Register from "../Screens/Signup/Register";
import RegistrationDetail from '../Screens/Signup/RegistrationDetail';
import Login from "../Screens/Signup/Login";

// Pets
import BookAppointment from '../Screens/Pet/BookAppointment';
import AddPet from '../Screens/Pet/AddPet';

//My Profile
import MyProfile from '../Screens/Profile/MyProfile';

// Appointments
import Appointments from '../Screens/Appointments/Appointments';
import AppointmentConfirmation from '../Screens/Pet/AppointmentConfirmation';

export const signUpNaviationOptions = createStackNavigator({
    Login: { screen: Login },
    RegistrationDetail: { screen: RegistrationDetail },
    Register: { screen: Register },
    AddPet: { screen: AddPet },
},{
    headerMode: 'none'
})

export const myProfileNavigationOptions = createStackNavigator({
    MyProfile: { screen: MyProfile}
  },
  { headerMode: 'none' }
  );

  export const petNavigationOptions = createStackNavigator({
    BookAppointment: { screen: BookAppointment },
    AppointmentConfirmation: { screen: AppointmentConfirmation},
    AddPet: { screen: AddPet },
  },
  { headerMode: 'none'}
  );

  export const appointmentsNavigationOptions = createStackNavigator({
    Appointments: { screen: Appointments}
  },
  { headerMode: 'none' }
  );

// Bottom Tab

export const bottomTabNavigator = createBottomTabNavigator(
    {
        "My Profile":{
            screen: myProfileNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/Bottom_icon/my_Profile.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        Pets:{
            screen: petNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/Bottom_icon/pets.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        Appointments:{
            screen: appointmentsNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/Bottom_icon/Appointments.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        "Invite friends":{
            screen: InviteFriends,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/Bottom_icon/invite_friends.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        }
    },
    {
        initialRouteName: 'Pets',        
        tabBarOptions: {
            activeTintColor: '#fff',
            inactiveTintColor: '#B4BECC',
            showLabel: true,
            showIcon: true,
            tabBarPosition: 'bottom',
            labelStyle: {
                fontSize: 12,
            },
            iconStyle:{
                width: 30,
                height: 30
            },
            style: {
                backgroundColor: '#5F5D70',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            },
            indicatorStyle: '#fff',
        }
    }
  );

  

  export const homeNavigationOptions = createStackNavigator({
    Home:{ screen: Home },
    List:{ screen: List },
    ShopDetail:{ screen: ShopDetail},
    ShowReviews: { screen: ShowReviews },
  },
  {
    headerMode: 'none'
    }
  )

export const appNavigationOptions = createStackNavigator({
    Splash: { screen: Splash},
    homeNavigationOptions: { screen: homeNavigationOptions},
    bottomTabNavigator: { screen: bottomTabNavigator },
    signUpNaviationOptions: { screen: signUpNaviationOptions },
    petNavigationOptions: { screen: petNavigationOptions }
},{
    headerMode: 'none'
})

export const AppContainer = createAppContainer(appNavigationOptions);