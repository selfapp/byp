import React, { Component } from 'react';
import gql from "graphql-tag";
import { Query } from "react-apollo";

export default class API extends Component {

    static request(query1, variable, callBack){
        var loadingStatus='', dataValue='', errorMessage='';
        {
            console.log(query1)
        }
        
        <Query query={query1} variables={variable}>
            {async ({loading, error, data}) => {
                console.log(data)
                loadingStatus=loading;
                dataValue=data;
                errorMessage=error;
                
                
                // callBack(loading, error, data)
            }}
        </Query>
        callBack(loadingStatus, dataValue, errorMessage)
    }


}