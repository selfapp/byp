import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
    TextInput,
} from 'react-native';

export class BoldText extends Component {
    render() {
        let {
            style
        } = this.props;
        if (style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{ color: '#212224', fontSize: 20, fontFamily: "Nunito-Bold"} ,style]}>{this.props.children}</Text>);
    }
}

export class LightText extends Component {
    render() {
        let {
            style
        } = this.props;
        if (style == null || style === undefined) {
            style = {}
        }
        return (<Text style={[{color: '#585858', fontFamily: "Nunito-Regular"}, style]}
                      numberOfLines={this.props.numberOfLines ? this.props.numberOfLines : null}>{this.props.children}</Text>);
    }
}

export class TextInputField extends Component {
    render(){
        let{
            style,
            containerStyle
        } = this.props;
        if(style == null || style == undefined) {
            style = {}
        }
        if( containerStyle == null || containerStyle == undefined){
            containerStyle = {}
        }
        return(
            <View style={[{ marginVertical: 10, height: 50, borderRadius: 5, flexDirection: 'row', alignItems: 'center', padding: 5, paddingHorizontal: 10, backgroundColor: '#fff'}, containerStyle]}>
                {(this.props.image) ? (
                    <Image
                        source = {this.props.image}
                    />
                ) : (null)}
                
                <TextInput
                    style={[{ marginLeft: 5, flex: 1, height: 40, fontFamily: "Nunito-Regular" }, this.props.style]}
                    secureTextEntry = {this.props.secureTextEntry}
                    editable={this.props.editable}
                    maxLength={this.props.maxLength}
                    placeholder={this.props.placeholder}
                    keyboardType={this.props.keyboardType}
                    onChangeText={this.props.onChangeText}
                />
            </View>
        )
    }
}
