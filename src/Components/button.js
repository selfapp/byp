import React, {Component}  from "react";
import { 
  TouchableOpacity,
  Image,
  View
 } from "react-native";
 import colors from '../styles/colors';
import {BoldText, LightText} from '../Components/styledTexts';

export class Button extends Component {
  render(){
  return (
    <TouchableOpacity
              style={[{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 40,
                  borderRadius: 20,
                  backgroundColor: this.props.color,
                  }, this.props.style]}
                  onPress = {this.props.onPress}
          >
            {this.props.Light ? (
              <LightText style={[{color: 'white'}, this.props.textStyle]}>{this.props.value}</LightText>
            ): (
              <BoldText style={[ {color: 'white', fontWeight: '500'}, this.props.textStyle]}>{this.props.value}</BoldText>
            )
          }  
         </TouchableOpacity>
  ) 
  }
};

export class RadioButton extends Component {
  render(){
    return(
      <TouchableOpacity style={[{ flexDirection: 'row', height: 40, justifyContent:'center', alignItems:'center' }, this.props.containerStyle]}
        onPress = {this.props.onPress}
      >
          <Image
            source = { (this.props.isSelected) ? require('../assets/radio_button.png') : require('../assets/radio_button_unchecked.png')}
          />
          <LightText style={{ marginLeft: 5 }}>{this.props.value}</LightText>
      </TouchableOpacity>
    )
  }
}

export class SelectButton extends Component {
  render(){
    return(
      <TouchableOpacity style={[{ height: 60, width: 120, margin: 5, justifyContent:'center', alignItems:'center', backgroundColor: (this.props.isSelected) ? colors.appColor : '#fff', borderWidth: .3, borderColor: colors.gray02, borderRadius: 5 }, this.props.containerStyle]}
        onPress = {this.props.onPress}
      >
        <BoldText style={[{ color: (this.props.isSelected) ? '#fff' : colors.textColor, fontWeight: '500', fontSize: 18 }, this.props.textStyle]}>{this.props.value}</BoldText>
          {/* <LightText style={{ marginLeft: 5 }}>{this.props.value}</LightText> */}
      </TouchableOpacity>
    )
  }
}
