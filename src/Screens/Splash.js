import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Dimensions,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';

const {height, width} = Dimensions.get('screen');

export default class Splash extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        AsyncStorage.getItem('token').then((data) => {
            if(data !== null){
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'bottomTabNavigator' })],
                  });
                  this.props.navigation.dispatch(resetAction);
            }else{
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'homeNavigationOptions' })],
                  });
                  this.props.navigation.dispatch(resetAction);
            }
        })
    }

    render(){
        return(
            <View style={{ flex: 1 }}>
                <ImageBackground
                 source = {require('../assets/background.png')}
                 style = {{ width: width, height: height}}
                >   
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Image
                    source = { require('../assets/byp-mobile-logo.png')}
                   />    
                </View> 
                </ImageBackground>
            </View>
        )
    }
}

