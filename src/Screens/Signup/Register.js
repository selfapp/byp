import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Dimensions,
    ScrollView,
    Image,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    Platform, 
    TouchableWithoutFeedback
} from 'react-native';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import AsyncStorage from '@react-native-community/async-storage';
import { 
    BoldText,
    TextInputField
    } from "../../Components/styledTexts";
import { Button } from "../../Components/button";
import colors from '../../styles/colors';
import Loader from '../../Components/Loader';
import ErrorHandler from '../../Components/ErrorHandler';

const {height, width} = Dimensions.get('screen');


export default class Register extends Component{
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
            passwordConfirmation:'',
            userDetail: '',
            petUUid:''
        }
    }
    componentDidMount(){
        AsyncStorage.getItem('petUUid').then((petUUid) => {
            this.setState({petUUid: petUUid});
        })
        this.setState({userDetail: this.props.navigation.state.params.userDetail})
        console.log(this.props.navigation.state.params.userDetail)
    }

    onSuccess=(data)=>{
        console.log(JSON.stringify(data))
        AsyncStorage.setItem('token',data.registerPetParent.accessToken).then(()=>{
            this.props.navigation.navigate('bottomTabNavigator')
        })
    }
    
    render(){
        return(
            <View style={{ flex: 1 }}>
                <ImageBackground
                 source = {require('../../assets/background.png')}
                 style = {{ width: width, height: height}}
                >
                 <Mutation mutation={REGISTATION} onError={(error) => {ErrorHandler.showError(error)}} onCompleted={(data) =>{this.onSuccess(data)}}>
                    {(register, { loading }) => (
                    <ScrollView style={{ flex: 1, paddingHorizontal: 30 }}>
                        <SafeAreaView style={{ flex: 1}}>
                        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <KeyboardAvoidingView
                            contentContainerStyle={{ flex: 1 }}
                            behavior='position'
                            keyboardVerticalOffset={-60}
                            enabled
                        >
                            <View style={{ height: height*0.4, justifyContent: 'space-evenly', }}>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent:'center'}}>
                                    <Image
                                        source = { require('../../assets/byp-mobile-logo.png')}
                                    />
                                </View>
                                <View style={{ flex: 1,  justifyContent: 'space-evenly'}}>
                                    <BoldText style={{ color: '#fff', textAlign: 'center'}}>Pet- Parent Register Below</BoldText>                                   
                                </View>
                            </View>
                            <View style={{ flex: 1, justifyContent:'space-evenly',  }}>
                                
                                <TextInputField
                                    image={require('../../assets/user.png')}
                                    placeholder={'Email Address'}
                                    keyboardType={'email-address'}
                                    onChangeText={(email)=>{this.setState({email: email})}}
                                />
                                <TextInputField
                                    image={require('../../assets/password.png')}
                                    placeholder={'Password'}
                                    secureTextEntry={true}
                                    onChangeText={(password)=>{ this.setState({ password: password})}}
                                />
                                <TextInputField
                                    image={require('../../assets/password.png')}
                                    placeholder={'Confirm Password'}
                                    secureTextEntry={true}
                                    onChangeText={(passwordConfirmation)=>{this.setState({ passwordConfirmation: passwordConfirmation})}}
                                />
                            </View>
                            <View style={{ flex: 1, marginTop: 30}}>
                                        <View style={{  marginTop: 10, height: 50, justifyContent:'center'}}>
                                            <Button
                                                style={{ height: 50, borderRadius: 25, padding: 5, marginHorizontal: 0, borderRadius: 5}}
                                                value={'NEXT'}
                                                color={colors.migGray}
                                                textStyle={{fontSize: 26, color:colors.white}}
                                                onPress={()=>{
                                                    const {email, password, passwordConfirmation, userDetail, petUUid} = this.state;
                                                    if(email && password && passwordConfirmation){
                                                        register({ variables: { input: {email: email, password: password, passwordConfirmation: passwordConfirmation, firstName: userDetail.firstName, lastName: userDetail.lastName, contactNumber: userDetail.phoneNumber, streetAddress: userDetail.streetAddress, city: userDetail.city, state: userDetail.state, zip: userDetail.zipCode, pets:[petUUid] }}});
                                                    }else{
                                                        alert('Please fill all details')
                                                    }
                                                }}
                                            />
                                            
                                        </View>  
                                    <View style={{ alignItems: 'center'}}>
                                    </View>
                            </View>
                            </KeyboardAvoidingView>
                            </TouchableWithoutFeedback>
                            </SafeAreaView>
                            <Loader loading={loading}/>
                        </ScrollView>
                        )}     
                        </Mutation>
                    </ImageBackground>
            </View>
        )
    }
}

const REGISTATION = gql`
mutation registerPetParent($input: RegisterPetParentInput!){
    registerPetParent(input: $input) {
                accessToken
            }
          }`;