import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Dimensions,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Image,
    SafeAreaView,
    Text,
    Platform
} from 'react-native';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import { 
    BoldText,
    TextInputField,
    LightText
     } from "../../Components/styledTexts";
import { Button } from "../../Components/button";
import colors from '../../styles/colors';
import Loader from '../../Components/Loader';
import ErrorHandler from '../../Components/ErrorHandler';
import { ScrollView } from 'react-native-gesture-handler';

const {height, width} = Dimensions.get('screen');


export default class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }
    onSuccess=(data)=>{
        console.log(JSON.stringify(data))
        AsyncStorage.setItem('token',data.signInPetParent.accessToken).then(()=>{
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'bottomTabNavigator' })],
              });
              this.props.navigation.dispatch(resetAction);
        })
    }

    render(){
        return(
            <View style={{ flex: 1 }}>
                <ImageBackground
                 source = {require('../../assets/background.png')}
                 style = {{ width: width, height: height}}
                >
                <Mutation mutation={SIGNIN_PET_PARENT} onError={(error) => {ErrorHandler.showError(error)}} onCompleted={(data) =>{this.onSuccess(data)}}>
                    {(signInPetParent, { loading }) => (
                        <SafeAreaView style={{ flex: 1, marginHorizontal: 20 }}>
                             <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                             <KeyboardAvoidingView
                                behavior={Platform.OS === "ios" ? "padding" : null}
                                style={{flex: 1}}
                                keyboardVerticalOffset={Platform.select({ios: -100, android: 20})}
                                >
                            <View style={{ flex: 1, justifyContent: 'space-evenly', }}>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent:'center'}}>
                                    <Image
                                        source = { require('../../assets/byp-mobile-logo.png')}
                                    />
                                </View>
                                <View style={{ alignItems: 'center'}}>
                                    <BoldText style={{ color: '#fff'}}>Pet-Parent Log in</BoldText>
                                </View>
                            </View>
                            <View style={{  alignItems:'center', justifyContent: 'flex-end', marginVertical: 10  }}>
                                <TextInputField
                                        image={require('../../assets/user.png')}
                                        placeholder={'Email Address'}
                                        keyboardType={'email-address'}
                                        onChangeText={(email)=> this.setState({ email: email})}
                                    />
                                <TextInputField
                                    image={require('../../assets/password.png')}
                                    placeholder={'Password'}
                                    secureTextEntry={true}
                                    onChangeText={(password)=> this.setState({ password: password})}
                                />
                                
                            </View>
                            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                                    <View style={{ flex: 1, justifyContent: 'flex-start'}}>
                                        <Button
                                                style={{ height: 45, borderRadius: 25, padding: 5, marginHorizontal: 0, borderRadius: 5, marginVertical: 0}}
                                                value={'Log In'}
                                                color={colors.migGray}
                                                textStyle={{fontSize: 20, color:colors.white}}
                                                onPress={()=>signInPetParent({ variables: { email: `${this.state.email}`, password: `${this.state.password}`} })}
                                            />
                                    </View>
                                    <View style={{ alignItems:'center', justifyContent: 'center'}}>
                                        <LightText style={{ color: '#fff'}}>OR</LightText>
                                    </View>
                                    <View style={{ flex: 1, height: 45, justifyContent:'center'}}>
                                        <Button
                                                style={{ height: 45, borderRadius: 25, padding: 5, marginHorizontal: 0, borderRadius: 5}}
                                                value={'Tell us a bit about your pet'}
                                                color={colors.migGray}
                                                textStyle={{fontSize: 20, color:colors.white}}
                                                onPress={()=> this.props.navigation.navigate('AddPet')}
                                                // onPress={()=>signInPetParent({ variables: { email: `${this.state.email}`, password: `${this.state.password}`} })}
                                            />
                                    </View>
                                    
                                    <View style={{ flex: 1, marginVertical: 10, alignItems: 'center'}}>
                                        <Text style={{fontWeight: '600', color: colors.migGray}} onPress={() => console.log('Navigate here')}>Forgot your Password</Text>
                                    </View>
                            </View>
                            <Loader loading={loading}/>
                            </KeyboardAvoidingView>
                            </TouchableWithoutFeedback>
                            </SafeAreaView>
                                                        
                        )}     
                        </Mutation>
                    </ImageBackground>
            </View>
        )
    }
}

const SIGNIN_PET_PARENT = gql`
mutation signInPetParent($email: String!, $password: String!){
    signInPetParent(email: $email, password: $password) {
                accessToken,
                id
            }
          }`;