import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Dimensions,
    ScrollView,
    Image,
    SafeAreaView,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    Keyboard
} from 'react-native';
import { 
    BoldText, 
    TextInputField
 } from "../../Components/styledTexts";
import { Button } from "../../Components/button";
import colors from '../../styles/colors';

const {height, width} = Dimensions.get('screen');


export default class RegistrationDetail extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            streetAddress: '',
            city: '',
            state: '',
            zipCode: ''
        }
    }

    onNext=()=>{
        const {firstName, lastName, phoneNumber, streetAddress, city, state, zipCode} = this.state;
        if (firstName && lastName && phoneNumber && streetAddress && city && state && zipCode) {
            this.props.navigation.navigate('Register', { userDetail: this.state})
        }else{
            alert('Please fill all details')
        }
    }

    render(){
        return(
            <View style={{ flex: 1 }}>
                <ImageBackground
                 source = {require('../../assets/background.png')}
                 style = {{ width: width, height: height}}
                >
                    <SafeAreaView style={{ flex: 1}}>
                    <ScrollView style={{ flex: 1, paddingHorizontal: 30, paddingVertical: 30 }}>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <KeyboardAvoidingView
                            contentContainerStyle={{ flex: 1 }}
                            behavior='position'
                            keyboardVerticalOffset={-130}
                            enabled
                        >
                            <View style={{ height: height*0.3, justifyContent: 'flex-end', paddingVertical: 20 }}>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent:'flex-end'}}>
                                    <Image
                                        source = { require('../../assets/byp-mobile-logo.png')}
                                    />
                                </View>
                                <View style={{ flex: 1,  justifyContent: 'flex-end'}}>
                                    <BoldText style={{fontSize:24, color: '#fff', textAlign: 'center', fontWeight: '800'}}>Tell us about yourself</BoldText>                                    
                                </View>
                            </View>
                            <View style={{ flex: 1, justifyContent:'space-evenly',  }}>
                                <TextInputField
                                    image={require('../../assets/user.png')}
                                    placeholder={'First Name'}
                                    onChangeText={(firstName)=> this.setState({ firstName: firstName })}
                                />
                                <TextInputField
                                    image={require('../../assets/user.png')}
                                    placeholder={'Last Name'}
                                    onChangeText={(lastName)=> this.setState({ lastName: lastName })}
                                />
                                <TextInputField
                                    image={require('../../assets/phone-no.png')}
                                    placeholder={'Phone No.'}
                                    maxLength={10}
                                    keyboardType={'number-pad'}
                                    onChangeText={(phoneNumber)=> this.setState({ phoneNumber: phoneNumber })}
                                />
                                <TextInputField
                                    image={require('../../assets/street-address.png')}
                                    placeholder={'Street Address'}
                                    onChangeText={(streetAddress)=> this.setState({ streetAddress: streetAddress })}
                                />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                <TextInputField
                                    containerStyle={{ flex: 1.5 }}
                                    placeholder={'City'}
                                    onChangeText={(city)=> this.setState({ city: city })}
                                />  
                                <TextInputField
                                    containerStyle={{ flex: 1, marginLeft: 5 }}
                                    placeholder={'State'}
                                    onChangeText={(state)=> this.setState({ state: state })}
                                />
                                <TextInputField
                                    containerStyle={{ flex: 1.2, marginLeft: 5}}
                                    placeholder={'Zip code'}
                                    maxLength={6}
                                    keyboardType={'number-pad'}
                                    onChangeText={(zipCode)=> this.setState({ zipCode: zipCode })}
                                />
                                </View>
                            </View>
                            <View style={{ flex: 1, marginTop: 30}}>
                                    <View style={{  marginTop: 10, height: 50, justifyContent:'center'}}>
                                        <Button
                                                style={{ height: 50, borderRadius: 25, padding: 5, marginHorizontal: 0, borderRadius: 5}}
                                                value={'NEXT'}
                                                color={colors.migGray}
                                                textStyle={{fontSize: 26, color:colors.white}}
                                                onPress={()=> this.onNext()}
                                            />
                                    </View>
                            </View>
                            <View style={{ height: 150 }}/>
                            </KeyboardAvoidingView>
                            </TouchableWithoutFeedback>
                        </ScrollView>
                        </SafeAreaView>
                    </ImageBackground>
            </View>
        )
    }
}
