import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Dimensions,
    Image
} from 'react-native';
import colors from '../../styles/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';

const {height, width} = Dimensions.get('screen');

export default class MyProfile extends Component{
    constructor(props){
        super(props);
    }
    onPressRight=()=>{
        AsyncStorage.clear();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Splash' })],
          });
          this.props.navigation.dispatch(resetAction);
    }
    
    render(){
        return(
            <View style={{ flex: 1 }}>
                <ImageBackground
                 source = {require('../../assets/background.png')}
                 style = {{ width: width, height: height}}
                >   
                 <View style={{flex: .09}}>
                    <Header
                        color={colors.appColor}
                        value={'My Profile'}
                        rightIcon={require('../../assets/logout.png')}
                        onPressRight={this.onPressRight}
                    />
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Image
                    source = { require('../../assets/byp-mobile-logo.png')}
                   />    
                </View> 
                </ImageBackground>
            </View>
        )
    }
}

