import React, { Component } from 'react';
import {
    View,
    ScrollView,
    FlatList,
    Image,
    TouchableOpacity,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Platform,
    Dimensions,
    ImageBackground
} from 'react-native';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Mutation } from "react-apollo";
import CalendarStrip from 'react-native-calendar-strip';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../Components/Header';
import colors from '../../styles/colors';
import General from '../../styles/General';
import Constant from '../../Components/Constant';
import Loader from '../../Components/Loader';
import ErrorHandler from '../../Components/ErrorHandler';
import { SelectButton, Button } from '../../Components/button';
import { BoldText, LightText } from '../../Components/styledTexts';


export default class BookAppointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedPet: 0,
            selectedTimeSlot: undefined,
            serviceType: { label: "Grooming", value: 'grooming' },
            petList: [{ name: 'add' }],
            serviceList: [],
            locationId: null,
            selectedDate: ''
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('locationId').then((data) => {
            this.setState({ locationId: data })
        })
    }
    petListResponse(petList) {
        console.log(petList)
        let pets = this.state.petList;
        pets = [...pets, ...petList.pets]
        if (pets.length > 1) {
            this.setState({ petList: pets, selectedPet: pets[1].id })
        }
    }
    serviceListResponse(serviceList) {
        console.log(JSON.stringify(serviceList))
        this.setState({ serviceList: serviceList.servicesForPet })
    }
    
    renderItem = ({ item }) => {
        // console.log(item)
        return (
            <View style={[General.Card, item.discount ? { overflow: 'hidden', width: '100%' } : { width: '100%' }]}>
                <ImageBackground source={item.discount ? require('../../assets/green_bg.png') : null} style={{ width: '100%', flex: 1, borderRadius: 10, }}>
                    <View>

                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', padding: 10, paddingHorizontal: 15 }}>
                            <View>
                                <BoldText style={[item.discount ? { color: 'white' } : {}]}>{item.name}</BoldText>
                            </View>
                            <View>
                                <BoldText>$ {item.price}</BoldText>
                            </View>
                        </View>
                        {
                            item.discount && <View style={{ flexDirection: 'row', flex: 1, padding: 10, justifyContent: 'space-between' }}>
                                <View>
                                    <LightText style={[item.discount ? { color: 'white' } : { color: colors.appColor }]}>{"Full Groom"}</LightText>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: 'center', paddingRight: 5 }}>
                                    <Image source={require('../../assets/gift.png')} />
                                    <LightText style={[item.discount ? { color: '#fbe085' } : { color: '#fbe085' }]}>+Taxes ${'50'}</LightText>
                                </View>
                            </View>
                        }
                        <View style={{ height: .6, backgroundColor: colors.gray02, marginVertical: 10, marginHorizontal: 10 }} />
                        <View style={{ paddingHorizontal: 10, paddingBottom: 10, flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
                            <View style={{ width: '60%' }}>
                                <LightText numberOfLines={2} style={[item.discount ? { color: 'white' } : {}]}>
                                    {item.description}
                                </LightText>
                            </View>
                            <View>
                                <View style={{ height: 50, justifyContent: 'center' }}>
                                    <Button
                                        style={{ height: 40, borderRadius: 25, paddingHorizontal: 10 }}
                                        value={'Add to cart'}
                                        color={colors.lightYellow}
                                        textStyle={{ fontSize: 12, color: colors.white }}
                                        onPress={() => {
                                            this.props.navigation.navigate('AppointmentConfirmation',{apointmentDetail:{selectedCategory: this.state.serviceType, serviceData: item, selectedPet: this.state.selectedPet}})
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                    </View>
                </ImageBackground>
            </View>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .09 }}>
                    <Header
                        color={colors.appColor}
                        value={'Book Appointment'}
                    />
                </View>

                <ScrollView style={{ flex: 1, marginHorizontal: 10 }}>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View>
                            <View style={{ padding: 5, paddingTop: 10 }}>
                                <BoldText>Select your pet</BoldText>
                            </View>
                            <View style={{ flex: 1 }}>

                                <Query query={getPetList} onCompleted={(data) => { this.petListResponse(data) }}>
                                    {({ loading }) => {
                                        if (loading) {
                                            return <Loader loading={loading} />
                                        }
                                        return <View />
                                    }}
                                </Query>
                                <FlatList
                                    data={this.state.petList}
                                    horizontal
                                    style={{ paddingVertical: 10 }}
                                    keyExtractor={(item, index) => index.toString()}
                                    ItemSeparatorComponent={() => {
                                        return <View style={{ flex: 1, width: 5 }} />
                                    }}
                                    renderItem={({ item }) => (
                                        <View style={[{ height: 150, width: 120, margin: 5, alignItems: 'center', justifyContent: 'center', }]}>
                                            {item.name === 'add' ? (
                                                <View style={{ flex: 1 }}>
                                                    <TouchableOpacity
                                                        style={[General.Card, { width: 120, height: 120, alignItems: 'center', justifyContent: 'center' }]}
                                                        onPress={() => this.props.navigation.navigate('AddPet')}
                                                    >
                                                        <Image
                                                            source={require('../../assets/upload-photo.png')}
                                                            style={{ height: 35, width: 35 }}
                                                            resizeMode={'contain'}
                                                        />

                                                    </TouchableOpacity>
                                                    <BoldText style={{ marginTop: 15, color: colors.textColor, fontWeight: '500', fontSize: 12, textAlign: 'center' }}>Add New Pet</BoldText>
                                                </View>
                                            ) : (
                                                    <View style={{ flex: 1 }}>
                                                        <TouchableOpacity style={[General.Card, { width: 120, height: 120, alignItems: 'center', justifyContent: 'center' }]}
                                                            onPress={() => this.setState({ selectedPet: item.id })}
                                                        >
                                                            <Image
                                                                source={{ uri: `${Constant.base_url}${item.image}` }}
                                                                style={{ height: 120, width: 120, borderRadius: 5 }}
                                                                resizeMode={'cover'}
                                                            />
                                                            {(this.state.selectedPet === item.id) ? (
                                                                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'center' }}>
                                                                    <Image
                                                                        source={require('../../assets/check.png')}
                                                                    />
                                                                </View>
                                                            ) : (null)}

                                                        </TouchableOpacity>
                                                        <BoldText style={{ marginTop: 15, color: colors.textColor, fontWeight: '500', fontSize: 12, textAlign: 'center' }}>{item.name}</BoldText>
                                                    </View>
                                                )}
                                        </View>
                                    )}
                                />


                                <View style={{ padding: 5 }}>
                                    <BoldText>Service Categories</BoldText>
                                    <View style={{ flex: 1 }}>

                                        <FlatList
                                            data={[{ label: "Grooming", value: 'grooming' }, { label: "Training", value: 'training' }, { label: "Walking", value: 'walking' }, { label: "Sitting", value: 'sitting' }]}
                                            horizontal
                                            keyExtractor={(item, index) => index.toString()}
                                            style={{ flex: 1, paddingVertical: 10 }}
                                            ItemSeparatorComponent={() => {
                                                return <View style={{ flex: 1, width: 5 }} />
                                            }}
                                            renderItem={({ item, index }) => (
                                                <SelectButton
                                                    key={index.toString()}
                                                    value={item.label}
                                                    isSelected={(this.state.serviceType.value === item.value)}
                                                    onPress={() => this.setState({ serviceType: item })}
                                                />
                                            )}
                                        />
                                    </View>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <View style={{ padding: 5 }}>

                                        <Query query={SERVICE_FOR_PET} variables={{ petId: this.state.selectedPet, locationId: this.state.locationId, kind: this.state.serviceType.value }} onCompleted={(data) => { this.serviceListResponse(data) }}>
                                            {({ loading }) => {
                                                if (loading) {
                                                    return <Loader loading={loading} />
                                                }
                                                return <View />
                                            }}
                                        </Query>

                                        <BoldText style={{ paddingBottom: 10 }}>Select Service</BoldText>
                                        {(this.state.serviceList.length > 0) ? (
                                            <FlatList
                                                data={this.state.serviceList}
                                                style={{ padding: 7 }}
                                                keyExtractor={(item, index) => index.toString()}
                                                ItemSeparatorComponent={() => {
                                                    return <View style={{ height: 10 }} />
                                                }}
                                                renderItem={this.renderItem}
                                            />
                                        ) : (null)}
                                    </View>

                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>

            </View>
        )
    }
}

const getPetList = gql`
        query pets{
            pets{
                id
                name
                image
            }
          }`;

const SERVICE_FOR_PET = gql` 
query servicesForPet($petId: ID!, $locationId: ID!, $kind: String!){
    servicesForPet(petId: $petId, locationId: $locationId, kind: $kind ){
        id
        name
        description
        duration,
        price
    }
  }`;