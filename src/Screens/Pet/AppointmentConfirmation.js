import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Keyboard,
    TouchableWithoutFeedback,
} from 'react-native';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Mutation } from "react-apollo";
import CalendarStrip from 'react-native-calendar-strip';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../Components/Header';
import colors from '../../styles/colors';
import General from '../../styles/General';
import Loader from '../../Components/Loader';
import moment from "moment";
import ErrorHandler from '../../Components/ErrorHandler';
import { SelectButton, Button } from '../../Components/button';
import { BoldText, LightText } from '../../Components/styledTexts';


export default class AppointmentConfirmation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTimeSlot: undefined,
            timeSlots: [],
            locationId: null,
            selectedDate: moment(new Date(Date.now())).format("MM/DD/YYYY"),
            apointmentDetail: null,
            serviceId:'',
            selectedPet: 0
        }
    }
    componentDidMount() {
        this.setState({apointmentDetail: this.props.navigation.state.params.apointmentDetail, serviceId: this.props.navigation.state.params.apointmentDetail.serviceData.id, selectedPet: this.props.navigation.state.params.apointmentDetail.selectedPet})
        AsyncStorage.getItem('locationId').then((data) => {
            this.setState({ locationId: data })
        })
    }
    
    
    onSuccess(data) {
        alert('Appointment created')
    }
    slotsResponse(data){
        console.log('Slots -->',JSON.stringify(data))
        this.setState({ timeSlots: data.availableSlots})
    }

    ListTimeSlot = () => {
        return this.state.timeSlots.map((item, index) => {
            return (
                    <SelectButton
                        key = {index.toString()}
                        value={item.slot}
                        textStyle={{fontSize: 14}}
                        isSelected={(this.state.selectedTimeSlot === item.slot)}
                        onPress={()=>this.setState({ selectedTimeSlot: item.slot })}
                        containerStyle={{ height: 50, width: '28%', margin: 7}}
                    />
            )
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .09 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        color={colors.appColor}
                        value={'Book Appointment'}
                    />
                </View>

                <ScrollView style={{ flex: 1, marginHorizontal: 10 }}>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View>
                            <View style={{ padding: 10, marginVertical: 10 }}>
                                <BoldText>You have selected service</BoldText>
                                <View style={[General.Card, { paddingVertical: 10, marginVertical: 15}]}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                                        <LightText>Service Categories</LightText>
                                        <BoldText style={{ fontSize: 16, color: colors.appColor}}>{this.state.apointmentDetail && this.state.apointmentDetail.selectedCategory.label} </BoldText>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                                        <LightText>Selected Service</LightText>
                                        <BoldText style={{ fontSize: 16, color: colors.appColor}}>{this.state.apointmentDetail && this.state.apointmentDetail.serviceData.name} </BoldText>
                                    </View>
                                    <View style={{ height: .5, backgroundColor: colors.gray02, marginHorizontal: 10}}/>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                                        <LightText>Total price</LightText>
                                        <BoldText style={{ fontSize: 16 }}>$ {this.state.apointmentDetail && this.state.apointmentDetail.serviceData.price} </BoldText>
                                    </View>
                                </View>
                            </View>
                           
                                <View style={{ flex: 1 }}>
                                    <View style={{ padding: 10 }}>
                                        <BoldText>Select a Day</BoldText>
                                    </View>
                                    <View style={{ marginTop: 10, flex: 1 }}>
                                        <CalendarStrip
                                            daySelectionAnimation={{ type: 'border', borderWidth: 1, borderHighlightColor: colors.appColor }}
                                            style={{ flex: 1, height: 100 }}
                                            calendarHeaderStyle={{ color: 'gray' }}
                                            calendarColor={'white'}
                                            dateNumberStyle={{ color: 'gray' }}
                                            dateNameStyle={{ color: 'black' }}
                                            minDate={new Date(Date.now())}
                                            onDateSelected={date => this.setState({ selectedDate: date.format("MM/DD/YYYY") })}
                                        />
                                    </View>
                                    <View style={{ height: 1, width: '100%', backgroundColor: '#D3D3D3', marginVertical: 20, marginHorizontal: 10 }} />

     {/* time slot*/}
                                    <View style={{ flex: 1 }}>
                                        {
                                            console.log(this.state.selectedPet, this.state.serviceId, this.state.selectedDate )
                                        }
                                    <Query query={AVAILABLE_SLOTS} variables={{petId: this.state.selectedPet, serviceId: this.state.serviceId, date: this.state.selectedDate}} onCompleted={(data) => { this.slotsResponse(data)  }} >
                                        {({ loading , error, data}) => {
                                            if (loading) {
                                                return <Loader loading={loading} />
                                            }
                                            if(error) {
                                                return <BoldText>{JSON.stringify(error)}</BoldText>
                                            }
                                            return <View />
                                        }}
                                    </Query>

                                        <View style={{ padding: 10 }}>
                                            <BoldText>Select Time Slot</BoldText>
                                            <View style={{ flexWrap: 'wrap', flexDirection: 'row', width: '100%', paddingTop: 15 }}>
                                                {
                                                    this.ListTimeSlot()
                                                }
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                <Mutation mutation={BOOK_APPOINTMENT} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                                {(bookAppointment, { loading }) => (
                                    <View style={{ flex: 1, marginTop: 20, height: 45, justifyContent:'center'}}>
                                        <Button
                                                style={{ height: 45, borderRadius: 25, padding: 5, marginHorizontal: 30, borderRadius: 5}}
                                                value={'ADD TO CART'}
                                                color={colors.appColor}
                                                textStyle={{fontSize: 20, color:colors.white}}
                                                onPress={()=> bookAppointment({ variables: { input: { serviceIds: [this.state.serviceId], locationId: this.state.locationId, petId: this.state.selectedPet, date: this.state.selectedDate, slot: this.state.selectedTimeSlot  } } })}
                                            />
                                    </View>
                                 )}
                                 </Mutation>
                                <View style={{ height: 50}}/>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>

            </View>
        )
    }
}

const AVAILABLE_SLOTS = gql`
        query availableSlots($petId: ID!, $serviceId: ID!, $date: Date!){
            availableSlots(petId: $petId, serviceId: $serviceId, date: $date) {
                slot
            }
          }`;

const BOOK_APPOINTMENT = gql`
    mutation bookAppointment($input: BookAppointmentInput!){
        bookAppointment(input: $input) {
                status
            }
          }`;