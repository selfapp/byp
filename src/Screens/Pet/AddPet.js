import React, { Component } from 'react';
import {
    View,
    ScrollView,
    FlatList,
    Image,
    TouchableOpacity,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    StyleSheet,
    SafeAreaView,
    Text
} from 'react-native';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import DatePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';
import RNPickerSelect from 'react-native-picker-select';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import Header from '../../Components/Header';
import General from '../../styles/General';
import colors from '../../styles/colors';
import Loader from '../../Components/Loader';
import ErrorHandler from '../../Components/ErrorHandler';
import { Button, RadioButton, SelectButton } from '../../Components/button';
import { BoldText, TextInputField } from '../../Components/styledTexts';

const SIZE_DATA = [
    {
        label: 'Short',
        value: 'short',
    },
    {
        label: 'Medium',
        value: 'medium',
    },
    {
        label: 'Large',
        value: 'large',
    }
];
const PET_TYPE = [
    {
        label: 'Dog',
        value: 'dog',
    },
    {
        label: 'Cat',
        value: 'cat',
    },
    {
        label: 'Horse',
        value: 'horse',
    }
];

export default class AddPet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            petName: '',
            petBreed: '',
            petBirthday: '',
            petWeight: '',
            gender: 'male',
            neutered: true,
            petCoat: 'short',
            vetName: '',
            vetPhone: '',
            petType: 'dog',
            petImages: ['add']
        }
    }

    openGalleryForDocuments(buttonIndex) {

        if (buttonIndex === 1) {
            ImagePicker.openPicker({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    let obj = { file: `data:${image.mime};base64,${image.data}` }
                    let images = this.state.petImages;
                    images = [...images, obj]
                    this.setState({ petImages: images })
                });
        } else if (buttonIndex === 0) {
            ImagePicker.openCamera({
                width: 1000,
                height: 1000,
                cropping: true,
                includeBase64: true,
                mediaType: 'photo'
            })
                .then(image => {
                    let obj = { file: `data:${image.mime};base64,${image.data}` }
                    let images = this.state.petImages;
                    images = [...images, obj]
                    this.setState({ petImages: images })
                });
        }
    }

    
    onSuccess = (data) => {
        console.log(JSON.stringify(data))
        
        AsyncStorage.getItem('token').then((token) => {
            if(token !== null){
                this.props.navigation.goBack()
            }else{
                console.log('data')
                console.log(JSON.stringify(data))
                AsyncStorage.setItem('petUUid',data.addPet.uuid).then(()=>{
                    this.props.navigation.navigate('RegistrationDetail')
                })
            }
        }) 
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .09 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        color={colors.appColor}
                        value={'Add new pets'}
                    />
                </View>
                    <Mutation mutation={ADD_PET} onError={(error) => { ErrorHandler.showError(error) }} onCompleted={(data) => { this.onSuccess(data) }}>
                        {(addPet, { loading }) => (
                            <SafeAreaView style={{ flex: 1 }}>
                            <ScrollView style={{ flex: 1 }}>

                                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                                    <KeyboardAvoidingView
                                        contentContainerStyle={{ flex: 1 }}
                                        behavior='position'
                                        keyboardVerticalOffset={-600}
                                        enabled
                                    >
                                        <View style={{ flex: 1, marginHorizontal: 10 }}>
                                            <View style={{ padding: 10, paddingVertical: 20 }}>
                                                <BoldText>Add a new pet below</BoldText>
                                            </View>
                                            <View style={{ flex: 1 }}>
                                                <FlatList
                                                    data={this.state.petImages}
                                                    horizontal
                                                    style={{ padding: 10 }}
                                                    ItemSeparatorComponent={() => {
                                                        return <View style={{ flex: 1, width: 5 }} />
                                                    }}
                                                    renderItem={({ item }) => (
                                                        <View style={[General.Card, { height: 100, width: 100, margin: 5, alignItems: 'center', justifyContent: 'center', }]}>
                                                            {item === 'add' ? (
                                                                <TouchableOpacity
                                                                    style={{ alignItems: 'center' }}
                                                                    onPress={() => this.ActionSheet.show()}
                                                                >
                                                                    <Image
                                                                        source={require('../../assets/upload-photo.png')}
                                                                        style={{ height: 35, width: 35 }}
                                                                        resizeMode={'contain'}
                                                                    />
                                                                    <BoldText style={{ marginTop: 10, color: colors.textColor, fontWeight: '500', fontSize: 12, textAlign: 'center' }}>Upload Image</BoldText>
                                                                </TouchableOpacity>
                                                            ) : (
                                                                    <Image
                                                                        source={{ uri: item.file }}
                                                                        style={{ height: 98, width: 98, borderRadius: 5 }}
                                                                        resizeMode={'contain'}
                                                                    />
                                                                )}
                                                        </View>
                                                    )}
                                                />
                                                {/* Action sheet */}
                                                <ActionSheet
                                                    ref={o => this.ActionSheet = o}
                                                    title={'Which one do you like ?'}
                                                    options={['Take photo', 'Choose from Library', 'Cancel']}
                                                    cancelButtonIndex={2}
                                                    onPress={(index) => { this.openGalleryForDocuments(index) }}
                                                />
                                            </View>

                                            <View style={{ flex: 1, padding: 10, paddingVertical: 40 }}>
                                                <View>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Enter details of your pet</BoldText>
                                                    <TextInputField
                                                        containerStyle={{ borderWidth: .3, borderColor: colors.gray02, height: 40 }}
                                                        placeholder={'Name'}
                                                        onChangeText={(petName) => this.setState({ petName: petName })}
                                                    />
                                                </View>
                                                <View style={{ flex: 1 }}>
                                                    <FlatList
                                                        data={PET_TYPE}
                                                        horizontal
                                                        style={{ flex: 1, paddingVertical: 10 }}
                                                        ItemSeparatorComponent={() => {
                                                            return <View style={{ flex: 1, width: 5 }} />
                                                        }}
                                                        renderItem={({ item }) => (
                                                            <SelectButton
                                                                value={item.label}
                                                                isSelected={(this.state.petType === item.value)}
                                                                onPress={()=>this.setState({ petType: item.value })}
                                                            />
                                                        )}
                                                    />
                                                </View>

                                                <View>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Your dog type</BoldText>
                                                    <TextInputField
                                                        containerStyle={{ borderWidth: .3, borderColor: colors.gray02, height: 40 }}
                                                        placeholder={'Breed'}
                                                        onChangeText={(petBreed) => this.setState({ petBreed: petBreed })}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                    <View style={{ flex: 1 }}>
                                                        <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Your pet coat</BoldText>
                                                        <RNPickerSelect
                                                            placeholder={{}}
                                                            items={SIZE_DATA}
                                                            onValueChange={value => {
                                                                this.setState({ petCoat: value })
                                                            }}
                                                            style={pickerSelectStyles}
                                                            value={this.state.petCoat}
                                                            Icon={() => {
                                                                return <Image
                                                                    source={require('../../assets/arrow.png')}
                                                                />;
                                                            }}
                                                        />
                                                    </View>
                                                    <View style={{ flex: .2 }} />
                                                    <View style={{ flex: 1 }}>
                                                        <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Your pet weight</BoldText>
                                                        <TextInputField
                                                            containerStyle={{ borderWidth: .3, borderColor: colors.gray02, height: 40 }}
                                                            placeholder={'0-15lbs'}
                                                            keyboardType={'decimal-pad'}
                                                            onChangeText={(petWeight) => this.setState({ petWeight: parseFloat(petWeight) })}
                                                        />
                                                    </View>
                                                </View>

                                                <View style={{ marginTop: 10 }}>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Your pet birthday</BoldText>
                                                    <View style={{ flex: 1, marginVertical: 10 }}>
                                                        <DatePicker
                                                            style={{ flex: 1, width: '100%' }}
                                                            customStyles={{ dateInput: { paddingHorizontal: 10, borderWidth: .3, borderRadius: 5, borderColor: colors.gray02, alignItems: 'flex-start' }, dateText: { color: colors.textColor } }}
                                                            date={this.state.petBirthday}
                                                            mode="date"
                                                            placeholder="MM/DD/YYYY"
                                                            format="MM/DD/YYYY"
                                                            confirmBtnText="Confirm"
                                                            cancelBtnText="Cancel"
                                                            showIcon={false}
                                                            maxDate={new Date()}
                                                            onDateChange={(date) => this.setState({ petBirthday: date })}
                                                        />
                                                    </View>
                                                </View>

                                                <View style={{ marginTop: 10 }}>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Gender</BoldText>
                                                    <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                        <RadioButton
                                                            containerStyle={{ borderWidth: .3, borderColor: colors.gray02, paddingHorizontal: 30, borderRadius: 5, width: 120 }}
                                                            value={'Male'}
                                                            isSelected={(this.state.gender === 'male') ? true : false}
                                                            onPress={() => this.setState({ gender: 'male' })}
                                                        />
                                                        <RadioButton
                                                            containerStyle={{ marginLeft: 20, borderWidth: .3, borderColor: colors.gray02, paddingHorizontal: 30, borderRadius: 5, width: 120 }}
                                                            value={'Female'}
                                                            isSelected={(this.state.gender === 'female') ? true : false}
                                                            onPress={() => this.setState({ gender: 'female' })}
                                                        />
                                                    </View>
                                                </View>
                                                <View style={{ marginTop: 10 }}>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>{(this.state.gender === 'female') ? `Spayed` : 'Neutered'}</BoldText>
                                                    <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                        <RadioButton
                                                            containerStyle={{ borderWidth: .3, borderColor: colors.gray02, paddingHorizontal: 30, borderRadius: 5, width: 120 }}
                                                            value={'Yes'}
                                                            isSelected={this.state.neutered}
                                                            onPress={() => this.setState({ neutered: true })}
                                                        />
                                                        <RadioButton
                                                            containerStyle={{ marginLeft: 20, borderWidth: .3, borderColor: colors.gray02, paddingHorizontal: 30, borderRadius: 5, width: 120 }}
                                                            value={'No'}
                                                            isSelected={!this.state.neutered}
                                                            onPress={() => this.setState({ neutered: false })}
                                                        />
                                                    </View>
                                                </View>
                                                <View style={{ marginTop: 10 }}>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Vet name</BoldText>
                                                    <TextInputField
                                                        containerStyle={{ borderWidth: .3, borderColor: colors.gray02, height: 40 }}
                                                        onChangeText={(vetName) => this.setState({ vetName: vetName })}
                                                    />
                                                </View>
                                                <View style={{ marginTop: 10 }}>
                                                    <BoldText style={{ color: colors.textColor, fontWeight: '500', fontSize: 18 }}>Vet Phone number</BoldText>
                                                    <TextInputField
                                                        maxLength={10}
                                                        keyboardType={'number-pad'}
                                                        containerStyle={{ borderWidth: .3, borderColor: colors.gray02, height: 40 }}
                                                        onChangeText={(vetPhone) => this.setState({ vetPhone: vetPhone })}
                                                    />
                                                </View>
                                                <View style={{ marginTop: 20, paddingHorizontal: 20, height: 50, justifyContent: 'center' }}>
                                                    <Button
                                                        style={{ height: 40, borderRadius: 25, padding: 5 }}
                                                        value={'ADD PET'}
                                                        color={colors.appColor}
                                                        textStyle={{ fontSize: 20, color: colors.white }}
                                                        onPress={()=> {
                                                            const {petName, petType, petBreed, petCoat, petWeight, petBirthday, gender, neutered, vetName, vetPhone, petImages } = this.state;
                                                            // console.log(' neutered',neutered)
                                                            if (petName && petBreed && petWeight && petBirthday && vetName && vetPhone && petImages.length > 1) {
                                                                addPet({ variables: { input: { name: petName, birthday: `${petBirthday}`, weight: petWeight, gender: gender, vetName: `${vetName}`, vetContactNumber: `${vetPhone}`, neutered: neutered, breed: `${petBreed}`, petType: `${petType}`, coat: `${petCoat}`, image: `${petImages[1].file}`}}})
                                                            }else{
                                                                alert('Please fill all details')
                                                            }
                                                        }}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </KeyboardAvoidingView>
                                </TouchableWithoutFeedback>
                               
                            </ScrollView>
                         <Loader loading={loading} />
                </SafeAreaView>
                )}
                    </Mutation>
            </View>
        )
    }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 8,
        paddingHorizontal: 10,
        marginTop: 10,
        borderWidth: .3,
        borderColor: colors.gray02,
        borderRadius: 4,
        color: colors.textColor,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.3,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    iconContainer: {
        top: 25,
        right: 10,
    },
});

const ADD_PET = gql`
    mutation addPet($input: AddPetInput!){
      addPet(input: $input) {
                uuid,
                name
            }
          }`;