import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Dimensions,
    Image
} from 'react-native';

const {height, width} = Dimensions.get('screen');

export default class Appointments extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return(
            <View style={{ flex: 1 }}>
                <ImageBackground
                 source = {require('../../assets/background.png')}
                 style = {{ width: width, height: height}}
                >   
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Image
                    source = { require('../../assets/byp-mobile-logo.png')}
                   />    
                </View> 
                </ImageBackground>
            </View>
        )
    }
}

