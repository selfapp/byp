import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    FlatList,
    Image,
    SafeAreaView,
    Modal,
    TouchableWithoutFeedback,
    Dimensions,
    ScrollView
} from 'react-native';
import StarRating from 'react-native-star-rating';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../Components/Header';
import colors from '../styles/colors';
import Loader from '../Components/Loader';
import { RadioButton } from '../Components/button';
import { BoldText, LightText } from '../Components/styledTexts';
import Constant from '../Components/Constant';
import API from '../Components/API';

const {height, width} = Dimensions.get('screen');

export default class List extends Component{
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            petType:1,
            service: "grooming",
            lat: 31.7860603,
            lng: -132.0853276
        }
    }

    componentDidMount(){
         this.setState({ service: this.props.navigation.state.params.service, lat: this.props.navigation.state.params.lat, lng: this.props.navigation.state.params.lng })
    }

    renderItem = ({ item}) =>{
        return(
            <View style={{ paddingVertical: 2}}>
            <TouchableOpacity style={{ flex: 1, paddingVertical: 15, flexDirection: 'row', borderRadius: 1, backgroundColor: '#F7F7F7',shadowOffset:{width:0, height:2}, shadowOpacity: 0.18, shadowRadius:1, shadowColor: '#451B2D', elevation: 3  }}
                 onPress={()=>this.props.navigation.navigate('ShopDetail',{shopId: item.id})}
                >
                <View style={{flex:3 , alignItems:'center', justifyContent:'center'}}>
                    <View style={{ flexDirection:'row'}}>
                    {
                        (item.business.profileImage != '') ?(
                        <Image
                        source = { {uri: `${Constant.base_url}${item.business.profileImage}`}}
                        style={{ height: 60, width: 60, margin: 10, borderRadius: 30,  }}
                        resizeMode={'stretch'}
                        />    
                        ): (
                          <Icon name="home" size={60} color={'#fff'} />
                        )
                      }
                    <View style={{ flex: 1 , margin: 2, justifyContent:'space-evenly' }}>
                        <BoldText style={{ fontSize: 16 }}>{item.name}</BoldText>
                        <LightText style={{ marginVertical: 2 }}>{item.address}</LightText>
                        <View style={{ flex: 1, flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{height: 25, width: 60}}>
                            <StarRating
                                style={{ flex: 1, justifyContent:'space-between' }}
                                disabled={true}
                                maxStars={5}
                                starSize={20}
                                rating={3.5}
                                emptyStar={'ios-star-outline'}
                                fullStar={'ios-star'}
                                halfStar={'ios-star-half'}
                                iconSet={'Ionicons'}
                                fullStarColor={'#FFBD4B'}
                                // selectedStar={(rating) => this.onStarRatingPress(rating)}
                            />
                        </View>
                        <View style={{ flexDirection: 'row'}}>
                            <Image
                                source = { require('../assets/baat.png')}
                                style={{ margin:2}}
                            />
                            <Image
                                source = { require('../assets/Tablet.png')}
                                style={{ margin:2}}
                            />
                            <Image
                                source = { require('../assets/Tablet_carr.png')}
                                style={{ margin:2}}
                            />
                        </View>

                        </View>
                    </View>
                    </View>
                </View>
            </TouchableOpacity>
            </View>
        );
    }
  
    empetyList =() =>{
        return (
        <View style={{ alignItems: "center", justifyContent: "center", marginTop: 20 }}>
        <BoldText style={{ color: "gray", justifyContent: "center", marginTop: 20, fontSize: 20 }}>
            No Items Found
        </BoldText>
        </View>
        );
    }
    
    onPressRight(){

    }
selectItem(index){

}

// Rernder Modal

renderModalContent() {
    return(
        <View style={{ height: height*0.8, width: width*0.9, padding:10,borderRadius:10, justifyContent: 'center', backgroundColor:colors.white}}>
            <View style={{ height: 50, alignItems:'center', justifyContent:'center'}}>
                <BoldText style={{ color: colors.appColor, fontSize: 24}}>Filters</BoldText>
            </View>
            <View style={{height: 1 , backgroundColor: 'gray'}}/>
            <ScrollView style={{ flex: 1, padding: 8 }}>
                <View style={{ marginTop: 5 }}>
                    <BoldText style={{ fontWeight: '500'}}>Pet Types</BoldText>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flex: 1, alignItems:'flex-start'}}>
                    <RadioButton
                        isSelected={(this.state.petType === 1) ? true : false}
                        value = {'Dog'}
                        onPress={()=>this.setState({petType: 1})}
                    />
                    <RadioButton
                        isSelected={(this.state.petType === 2) ? true : false}
                        value = {'Small Critters'}
                        onPress={()=>this.setState({petType: 2})}
                    />
                    </View>
                    <View style={{flex: 1, alignItems:'flex-start'}}>
                    <RadioButton
                        isSelected={(this.state.petType === 3) ? true : false}
                        value = {'Cat'}
                        onPress={()=>this.setState({petType: 3})}
                    />
                    <RadioButton
                        isSelected={(this.state.petType === 4) ? true : false}
                        value = {'Birds'}
                        onPress={()=>this.setState({petType: 4})}
                    />
                    </View>
                    <View style={{flex: 1, alignItems:'flex-start'}}>
                    <RadioButton
                        isSelected={(this.state.petType === 5) ? true : false}
                        value = {'Horse'}
                        onPress={()=>this.setState({petType: 5})}
                    />
                    </View>  
                </View>
                <View style={{ marginTop: 5 }}>
                    <BoldText style={{ fontWeight: '500'}}>Pet Size</BoldText>
                </View>
                <TouchableOpacity style={{ marginVertical: 10, borderWidth: 1}}>
                    <LightText>Medium</LightText>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}


    render(){

        return(
            <View style={{flex: 1, backgroundColor:'#F5F5F5'}}>
                <View style={{flex: .09}}>
                    <Header
                    leftNavigation={ this.props.navigation}
                    color={colors.appColor}
                    value={'List'}
                    rightIcon={require('../assets/filter.png')}
                    onPressRight={this.onPressRight}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <SafeAreaView style={{flex: 1}}>
                        
                        <Query query={getServices} variables={{kind: this.state.service, lat: this.state.lat, lng: this.state.lng}}>
                            {({loading, error, data}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText>{error}</BoldText>
                                }
                                console.log(data)
                                if(data) {
                                    if(data){
                                    return <FlatList
                                            data = {data.availableLocations}
                                            keyExtractor = {(item, index) => index.toString()}
                                            style={{ flex: 1 }}
                                            renderItem = {this.renderItem}
                                            ListEmptyComponent = { this.empetyList}
                                        />
                                    }else return <BoldText></BoldText>
                                } 
                                
                            }}
                        </Query>
                        <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.setState({modalVisible: false})
                            }}>
                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.67)' }}>
                                    <TouchableWithoutFeedback>
                                        {this.renderModalContent()}
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </SafeAreaView>
                </View>
            </View>
        )
    }
}

const getServices = gql`
        query avail($kind: String!, $lat: Float!, $lng: Float!){
            availableLocations(kind: $kind, lat: $lat, lng: $lng) {
                id
                name
                address
                business{
                profileImage
                }
            }
          }`;