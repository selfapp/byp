import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Dimensions,
    Image
} from 'react-native';
import Header from '../Components/Header';
import colors from '../styles/colors';
import General from "../styles/General";
import { BoldText, LightText } from '../Components/styledTexts';
import { Button } from '../Components/button';

const {height, width} = Dimensions.get('screen')

  
export default class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            lat: 0,
            lng: 0
        }
    }
componentDidMount(){
    this.getLocation();   
}

getLocation(){
    navigator.geolocation.getCurrentPosition(
        (position) => {
            this.setState({
                lat: position.coords.latitude,
                lng: position.coords.longitude
            });
        },
        (error) => {
            alert('Please allow access to location services.')
            // alert(error.message)
    },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
    );
}

    goToList(service){
        this.getLocation();
        if(this.state.lat != 0 && this.state.lng != 0){
            console.log('lat-------', this.state.lat)
            console.log('lng---------', this.state.lng)
            this.props.navigation.navigate('List', {service: service, lat: this.state.lat, lng: this.state.lng})
        }
    }


    render(){

        return(
            <View style={{flex: 1, backgroundColor:'#F5F5F5'}}>
                <View style={{flex: .09}}>
                    <Header
                    color={colors.appColor}
                    value={'Home'}
                    />
                </View>
                <View style={{flex:1, justifyContent:'space-evenly'}}>
                    <View style={{ flex: 0.6, alignItems:'center', justifyContent:'center'}}>
                        <BoldText style={{fontSize:24, color: colors.textColor, fontWeight: '800'}}>Book service for Your Pet</BoldText>
                    </View>
                    <View style={{ flex: 2, justifyContent:'space-evenly'}}>
                    <View style={{flex:1, flexDirection: 'row' , justifyContent:'space-evenly'}}>
                        <TouchableOpacity style={[ General.Card, {  height: height * 0.5 / 2, width: width * 0.8 / 2, alignItems:'center', justifyContent:'center'}]}
                           onPress = {()=>this.goToList('grooming') }
                        >
                            <Image
                                source = {require('../assets/Grooming.png')}
                            />
                            <LightText style={{ marginTop:5, color: colors.serviceTextColor, fontSize: 16 }}>Grooming</LightText>
                        </TouchableOpacity>
                        <TouchableOpacity style={[ General.Card, {  height: height * 0.5 / 2, width: width * 0.8 / 2, alignItems:'center', justifyContent:'center'}]}
                            onPress = {()=>this.goToList('sitting') }
                        >
                            <Image
                                source = {require('../assets/PetCare.png')}
                            />
                            <LightText style={{ marginTop:5, color: colors.serviceTextColor, fontSize: 16 }}>Sitting</LightText>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1, flexDirection: 'row' , justifyContent:'space-evenly'}}>
                        <TouchableOpacity style={[ General.Card, {  height: height * 0.5 / 2, width: width * 0.8 / 2, alignItems:'center', justifyContent:'center'}]}
                            onPress = {()=>this.goToList('walking') }
                        >
                            <Image
                                source = {require('../assets/GroupClass.png')}
                            />
                            <LightText style={{ marginTop:5, color: colors.serviceTextColor, fontSize: 16 }}>Walking</LightText>
                        </TouchableOpacity>
                        <TouchableOpacity style={[ General.Card, {   height: height * 0.5 / 2, width: width * 0.8 / 2, alignItems:'center', justifyContent:'center'}]}
                            onPress = {()=>this.goToList('training') }
                        >
                            <Image
                                source = {require('../assets/Training.png')}
                            />
                            <LightText style={{ marginTop:5, textAlign: 'center', color: colors.serviceTextColor, fontSize: 16}}>Training</LightText>
                        </TouchableOpacity>
                    </View>
                    </View>
                    <View style={{height: height*0.1}}/>
                    
                </View>
            </View>
        )
    }
}
