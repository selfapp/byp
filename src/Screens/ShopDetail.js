import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Dimensions,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    TouchableWithoutFeedback,
    Modal,
    Keyboard
} from 'react-native';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import StarRating from 'react-native-star-rating';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../Components/Header';
import colors from '../styles/colors';
import General from "../styles/General";
import { BoldText, LightText } from '../Components/styledTexts';
import { Button } from '../Components/button';
import Review from '../Components/Review';
import Loader from '../Components/Loader';
import Constant from '../Components/Constant';

const {height, width} = Dimensions.get('screen')

export default class ShopDetail extends Component{
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            shopId: 3
        }
    }

    componentDidMount(){
        this.setState({shopId: this.props.navigation.state.params.shopId})
    }

    renderModalContent() {
        return(
            <TouchableWithoutFeedback
                onPress={()=>Keyboard.dismiss()}
            >
                <View style={{ padding:10,borderRadius:10, width: width-40, justifyContent: 'center', backgroundColor:colors.white}}>
                    <View style={{paddingVertical:10}}>
                        <BoldText style={{fontSize: 20, color: colors.appColor, fontWeight: '600', marginVertical: 10, alignSelf: 'center'}}>Add review</BoldText>
                    </View>
                <View>
                <View style={{ marginVertical:20, marginHorizontal: 5}}>
                    <BoldText>Rating</BoldText>
                    <View style={{height: 25, flexDirection:'row', alignItems:'center', }}>
                        <StarRating
                            style={{ flex: 1 }}
                            disabled={true}
                            maxStars={5}
                            starSize={20}
                            rating={3.5}
                            emptyStar={'ios-star-outline'}
                            fullStar={'ios-star'}
                            halfStar={'ios-star-half'}
                            iconSet={'Ionicons'}
                            fullStarColor={colors.lightYellow}
                        />
                    </View>

                    <View style={{ margin: 10, paddingVertical: 10}}>
                        <BoldText>Write your review</BoldText>
                            <TextInput
                            style={{padding: 3, marginTop: 10, height: 150, textAlignVertical: 'top', fontSize:16, alignItems:"flex-start", backgroundColor:'white', borderRadius: 5, borderWidth:1, borderColor:'gray'}} 
                            placeholder ="Type your review here."
                            returnKeyType='next'
                            autoCorrect={false}
                            multiline={true}
                            keyboardType='default'
                            // value={this.state.feedback}
                            // onChangeText={(feedback) => this.setState({feedback})}
                        />
                        </View>
                    </View>
                   </View>
                    <View style={{ height:50, alignItems: 'flex-end'}}>
                        <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center', padding: 14, backgroundColor: colors.appColor, borderRadius: 5}}
                          onPress={()=>this.setState({modalVisible:false})}
                        >
                            <BoldText style={{color:colors.white, fontSize: 16}}>Update Results</BoldText>
                        </TouchableOpacity>
                        
                    </View>
            </View>
            </TouchableWithoutFeedback>
        )
    }

    renderPhotos(photos){
        var item = [];
        for(var index = 1; index < 5; index++){
            if(photos.length > index){
            item.push(<Image
                source = {{uri: `${Constant.base_url}${photos[index].image}`}}
                style = {{ margin: 6, height: width*.8/4, borderRadius: 5, width: width*.8/4}}
            />)
            } 
        }
        return item

    }


    render(){
        return(
            <View style={{ flex: 1 }}>

                    <Query query={getServiceDetail} variables={{id: this.state.shopId}}>
                            {({loading, error, data}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText >{error}</BoldText>
                                }
                                console.log(data)
                                if(data) {
                                    if(data){
                                    return  <ScrollView style={{ flex: 1 }}>
                                    <View style={{}}>
                                        <ImageBackground
                                            source = {{uri: `${Constant.base_url}${data.locationDetails.business.coverImage}`}}
                                            style = {{ width: width, }}
                                        >
                                          <View style={{ flex: 1,  backgroundColor:'rgba(54,59,62,0.8)', paddingVertical: 5}}>
                                            <View style={{ flex: 0.3, height: 60 }}>
                                                <Header
                                                    leftNavigation={ this.props.navigation}
                                                    color={colors.appColor}
                                                    value={'Shop detail'}
                                                    backgroundColor = {'#00000000'}
                                                    rightIcon={require('../assets/email-material.png')}
                                                    onPressRight={()=>console.log('')}
                                                />
                                            </View>
                                            <View style={{ flex: 1, alignItems:'center', justifyContent: 'space-evenly'}}>
                                                <View style={{  alignItems:'center', justifyContent: 'center'}}>
                                                    <View style={{ marginVertical: 15, marginTop: 30, height: 100, width: 100, borderRadius: 50, alignItems:'center', justifyContent: 'center', backgroundColor: '#fff'}}>
                                                        <Image
                                                            source = {{uri: `${Constant.base_url}${data.locationDetails.business.profileImage}`}}
                                                            style={{height: 90, width: 90, borderRadius: 45 }}
                                                            resizeMode={'contain'}
                                                        />
                                                    </View>
                                                </View>
                                                <View style={{ marginVertical:10, alignItems: 'center', justifyContent: 'space-between'}}>
                                                    <BoldText style={{ color: '#fff', fontWeight: '500', fontSize: 26}}>{data.locationDetails.name}</BoldText>
                                                    <LightText style={{ color: '#fff', fontSize: 16}}>{data.locationDetails.address}</LightText>
                                                </View>
                                                <View style={{ marginVertical: 4, flexDirection: 'row', alignItems:'center', justifyContent: 'space-between'}}>
                                                    <View style={{height: 25, flexDirection:'row', alignItems:'center', justifyContent: 'center'}}>
                                                        <StarRating
                                                            style={{ flex: 1 }}
                                                            disabled={true}
                                                            maxStars={5}
                                                            starSize={20}
                                                            rating={3.5}
                                                            emptyStar={'ios-star-outline'}
                                                            fullStar={'ios-star'}
                                                            halfStar={'ios-star-half'}
                                                            iconSet={'Ionicons'}
                                                            fullStarColor={'#fff'}
                                                        />
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>4.2/5.0</LightText>
                                                    </View>
                                                    {/* <View style={{ marginHorizontal: 10}}>
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>20 miles NearBy</LightText>
                                                    </View> */}
                                                </View>
                                                <View style={{ marginVertical: 15, flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                                                    <View style={{ flexDirection: 'row', flex: 1, alignItems:'center', justifyContent: 'center'}}>
                                                        <Image
                                                            source = {require('../assets/Phone-simple-line-icons.png')}
                                                        />
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>{data.locationDetails.contactNumber}</LightText>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', flex: 1, alignItems:'center', justifyContent: 'center'}}>
                                                        <Image
                                                            source = {require('../assets/Envelope-open-simple-line-icons.png')}
                                                        />
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>sydnee_brekke@jean.net</LightText>
                                                    </View>
                                                </View>
                                            </View>
                                         </View>
                                        </ImageBackground>
                                    </View>
                                    <View style={{ flex: 1}}>
                                        <View style={{ margin: 10}}>
                                            <View style={{ marginVertical: 5}}>
                                                <BoldText>About us</BoldText>
                                            </View>
                                            <View>
                                                <LightText style={{ textAlign: 'justify'}}>{data.locationDetails.business.description}</LightText>
                                            </View>
                                        </View>
                                        <View style={{ margin: 10}}>
                                            <View style={{ marginVertical: 10}}>
                                                <BoldText>Services</BoldText>
                                            </View>
                                            <View style={{flex:1, flexDirection: 'row' , marginVertical: 10}}>
                                                <View style={[ General.Card, { height: width * 0.6 / 3, width: width * 0.8 / 3, alignItems:'center', justifyContent:'center'}]}>
                                                    <Image
                                                        source = {require('../assets/Grooming.png')}
                                                        style={{ height: 40, width: 40}}
                                                        resizeMode={'contain'}
                                                    />
                                                </View>
                                                <View style={[ General.Card, {marginLeft: 10, height: width * 0.6 / 3, width: width * 0.8 / 3, alignItems:'center', justifyContent:'center'}]}>
                                                    <Image
                                                        source = {require('../assets/PetCare.png')}
                                                        style={{ height: 40, width: 40}}
                                                        resizeMode={'contain'}
                                                    />
                                                </View>
                                                <View style={[ General.Card, { marginLeft: 10, height: width * 0.6 / 3, width: width * 0.8 / 3, alignItems:'center', justifyContent:'center'}]}>
                                                    <Image
                                                        source = {require('../assets/GroupClass.png')}
                                                        style={{ height: 40, width: 40}}
                                                        resizeMode={'contain'}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ margin: 10}}>
                                            <View style={{flex:1, flexDirection: 'row', marginVertical: 10, marginLeft: 10}}>
                                                <View style={{ paddingVertical: 2, justifyContent:'center', backgroundColor: colors.appColor}}>
                                                    <View style={{ flex: 1, backgroundColor: '#fff', alignItems:'center', justifyContent: 'center'}}>
                                                        <BoldText style={{ fontWeight: '500', fontSize: 22, marginHorizontal: 5, marginVertical: 10}}>FullBath</BoldText>
                                                    </View>
                                                </View>
                                                <View style={{ marginLeft: 10, paddingVertical: 2, justifyContent:'center', backgroundColor: colors.appColor}}>
                                                    <View style={{ flex: 1, backgroundColor: '#fff', alignItems:'center', justifyContent: 'center'}}>
                                                        <BoldText style={{ fontWeight: '500', fontSize: 22, marginHorizontal: 5, marginVertical: 10}}>Groom</BoldText>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ marginVertical: 25, justifyContent: 'center'}}>
                                        <View style={{ height: 80, justifyContent:'center', paddingHorizontal: 30}}>
                                             <Button
                                                    style={{ height: 60, borderRadius: 30}}
                                                    value={'REQUEST AN APPOINTMENT'}
                                                    color={colors.appColor}
                                                    textStyle={{fontSize: 18, color:colors.white}}
                                                    onPress ={() =>{
                                                        AsyncStorage.setItem('locationId',data.locationDetails.id).then(()=>{
                                                            this.props.navigation.navigate('signUpNaviationOptions')
                                                        })
                                                    }}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ margin: 10}}>
                                            <View style={{ marginVertical: 5}}>
                                                <BoldText>Store Photos</BoldText>
                                            </View>
                                            <View style={{ flex: 1 }}>
                                                {(data.locationDetails.business.businessImages.length > 0) ? (
                                                    <View style={{ flex: 1 }}>
                                                    <View style={{ padding: 2}}>
                                                    <Image
                                                        source = {{uri: `${Constant.base_url}${data.locationDetails.business.businessImages[0].image}`}}
                                                        style = {{ height: 200, borderRadius: 5}}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', padding: 2}}>
                                                    {
                                                        this.renderPhotos(data.locationDetails.business.businessImages)
                                                    }
                                                    </View>
                                                </View>
                                                ) : (null)}

                                            </View>
                                        </View>
                                        <View style={{ margin: 10}}>
                                            <View style={{ marginVertical: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                                <BoldText>Latest review</BoldText>
                                                <View style={{ height: 50, justifyContent: 'center' }}>
                                                    <Button
                                                            style={{ height: 50, borderRadius: 25, padding: 5, marginRight: 0}}
                                                            value={'ADD REVIEW'}
                                                            color={colors.lightYellow}
                                                            textStyle={{fontSize: 16, color:colors.white, padding: 10}}
                                                            onPress={()=>this.setState({modalVisible:true})}
                                                        />
                                                    </View>
                                            </View>
                                        </View>
                                        <View style={{ margin: 10}}>
                                            <Review
                                                image={require('../assets/Grooming.png')}
                                                name={'Name'}
                                                star={2.6}
                                                time={'2 hours ago'}
                                                comment={'Lorem Ipsum is simply dummy text of the printing.'}
                                            />
                                            <View style={{ marginVertical: 15, height: 0.5, backgroundColor: colors.gray03}}/>
                                            <Review
                                                image={require('../assets/Grooming.png')}
                                                name={'Abc'}
                                                star={3.4}
                                                time={'5 hours ago'}
                                                comment={`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`}
                                            />
                                            <View style={{ marginVertical: 15, height: 0.5, backgroundColor: colors.gray03}}/>
                                        
                                            <View style={{  marginTop: 20, height: 50, justifyContent:'center'}}>
                                                    <Button
                                                            style={{ height: 50, borderRadius: 25, padding: 5}}
                                                            value={'Load More'}
                                                            color={colors.appColor}
                                                            textStyle={{fontSize: 20, color:colors.white}}
                                                            // onPress={()=>this.props.navigation.navigate('ShowReviews', {shopId: this.props.navigation.state.params.shopId})}
                                                        />
                                             </View>
                                        
                                        </View>
                
                                    <View style={{ height: 50}}/>
                                    </View>
                                    
                                    <Modal animationType="fade" transparent={true} visible={this.state.modalVisible ? true : false}>
                                            <TouchableWithoutFeedback onPress={() => {
                                                this.setState({modalVisible: false})
                                            }}>
                                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.67)', padding:15 }}>
                                                    <TouchableWithoutFeedback>
                                                        {this.renderModalContent()}
                                                    </TouchableWithoutFeedback>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        </Modal>
                
                                </ScrollView>
                
                                    }else return <BoldText></BoldText>
                                } 
                                
                            }}
                        </Query>

                           </View>
        )
    }
}

const getServiceDetail = gql`
        query avail($id: ID!){
            locationDetails(id: $id) {
                id
                name
                address
                contactNumber
                business{
                profileImage
                description
                coverImage
                businessImages{
                    id
                    image
                  }
                }
            }
          }`;