import React, { Component } from 'react';
import {
    View,
    Dimensions,
    ImageBackground,
    Image,
    FlatList,
    SafeAreaView
} from 'react-native';
import StarRating from 'react-native-star-rating';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Header from '../Components/Header';
import colors from '../styles/colors';
import { BoldText, LightText } from '../Components/styledTexts';
import Review from '../Components/Review';
import Constant from '../Components/Constant';

const {height, width} = Dimensions.get('screen')

const data = [
    {
        name:`PUP'N SUDA`,
        address: `3767 Bo Falls, LA`,
        image:require('../assets/PetCare.png')
    },
    {
        name:`PUP'N SUDA`,
        address: `3767 Bo Falls, LA`,
        image:require('../assets/PetCare.png')
    },
    {
        name:`PUP'N SUDA`,
        address: `3767 Bo Falls, LA`,
        image:require('../assets/PetCare.png')
    },
    {
        name:`PUP'N SUDA`,
        address: `3767 Bo Falls, LA`,
        image:require('../assets/PetCare.png')
    },
    {
        name:`PUP'N SUDA`,
        address: `3767 Bo Falls, LA`,
        image:require('../assets/PetCare.png')
    },
    {
        name:`PUP'N SUDA`,
        address: `3767 Bo Falls, LA`,
        image:require('../assets/PetCare.png')
    }
]



export default class ShowReviews extends Component{
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            shopId: 0,
        }
    }
    componentDidMount(){
        this.setState({shopId: this.props.navigation.state.params.shopId})
    }

    renderItem = ({ item}) =>{
        return(
            <View style={{ paddingVertical: 2}}>
            <View style={{ flex: 1, paddingVertical: 15, flexDirection: 'row', borderRadius: 1, backgroundColor: '#F7F7F7',shadowOffset:{width:0, height:2}, shadowOpacity: 0.18, shadowRadius:1, shadowColor: '#451B2D', elevation: 3  }} >
                <Review
                    image={require('../assets/Grooming.png')}
                    name={'Name'}
                    star={2.6}
                    time={'2 hours ago'}
                    comment={'Lorem Ipsum is simply dummy text of the printing.'}
                />
            </View>
            </View>
        );
    }


    render(){
        return (
        <View style={{ flex: 1 }}>

                    <Query query={getServiceDetail} variables={{id: this.state.shopId}}>
                            {({loading, error, data}) => {
                                if(loading){
                                    return <Loader loading={loading} />
                                }
                                if(error) {
                                    return <BoldText >{error}</BoldText>
                                }
                                console.log(data)
                                if(data) {
                                    if(data){
                                    return  <View style={{}}>
                                        <ImageBackground
                                            source = {{uri: `${Constant.base_url}${data.locationDetails.business.coverImage}`}}
                                            style = {{ width: width, height: height*0.4,}}
                                        >
                                        <View style={{ flex: 1, backgroundColor:'rgba(54,59,62,0.5)'}}>
                                            <View style={{ flex: 0.3}}>
                                                <Header
                                                    leftNavigation={ this.props.navigation}
                                                    color={colors.appColor}
                                                    value={'Shop detail'}
                                                    backgroundColor = {'#00000000'}
                                                    rightIcon={require('../assets/email-material.png')}
                                                />
                                            </View>
                                            <View style={{ flex: 1, alignItems:'center', justifyContent: 'space-evenly'}}>
                                                <View style={{ flex: 2.5, alignItems:'center', justifyContent: 'center'}}>
                                                    <View style={{ height: 100, width: 100, borderRadius: 50, alignItems:'center', justifyContent: 'center', backgroundColor: '#fff'}}>
                                                        <Image
                                                            source = {{uri: `${Constant.base_url}${data.locationDetails.business.profileImage}`}}
                                                            style={{height: 90, width: 90, borderRadius: 45, backgroundColor: 'pink'}}
                                                            resizeMode={'contain'}
                                                        />
                                                    </View>
                                                </View>
                                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between'}}>
                                                    <BoldText style={{ color: '#fff', fontWeight: '500', fontSize: 26}}>{data.locationDetails.name}</BoldText>
                                                    <LightText style={{ color: '#fff', fontSize: 20}}>{data.locationDetails.address}</LightText>
                                                </View>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems:'center', justifyContent: 'space-between'}}>
                                                    <View style={{height: 25, flexDirection:'row', alignItems:'center', justifyContent: 'center'}}>
                                                        <StarRating
                                                            style={{ flex: 1 }}
                                                            disabled={true}
                                                            maxStars={5}
                                                            starSize={20}
                                                            rating={3.5}
                                                            emptyStar={'ios-star-outline'}
                                                            fullStar={'ios-star'}
                                                            halfStar={'ios-star-half'}
                                                            iconSet={'Ionicons'}
                                                            fullStarColor={'#fff'}
                                                        />
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>4.2/5.0</LightText>
                                                    </View>
                                                    <View style={{ marginHorizontal: 10}}>
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>1.3 KM NEARBY</LightText>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                                                    <View style={{ flexDirection: 'row', flex: 1, alignItems:'center', justifyContent: 'center'}}>
                                                        <Image
                                                            source = {require('../assets/Phone-simple-line-icons.png')}
                                                        />
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>{data.locationDetails.contactNumber}</LightText>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', flex: 1, alignItems:'center', justifyContent: 'center'}}>
                                                        <Image
                                                            source = {require('../assets/Envelope-open-simple-line-icons.png')}
                                                        />
                                                        <LightText style={{color: '#fff', marginLeft: 5}}>sydnee_brekke@jean.net</LightText>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        </ImageBackground>
                                    </View>
                                    }else return <BoldText></BoldText>
                                } 
                                }}
                    </Query>
                    <View style={{flex: 1, marginHorizontal: 5}}>
                        <View style={{marginVertical: 10}}>
                            <BoldText>Latest review</BoldText>
                        </View>
                        <SafeAreaView style={{ flex: 1}}>
                            <FlatList
                                data = {data}
                                keyExtractor = {(item, index) => index.toString()}
                                style={{ flex: 1 }}
                                renderItem = {this.renderItem}
                                />
                        </SafeAreaView>
                    </View>
            </View>
            )
    }
}

const getServiceDetail = gql`
        query avail($id: ID!){
            locationDetails(id: $id) {
                id
                name
                address
                contactNumber
                business{
                profileImage
                description
                coverImage
                businessImages{
                    id
                    image
                  }
                }
            }
          }`;