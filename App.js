/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View } from 'react-native';
import { AppContainer } from './src/Components/Router';
import { ApolloProvider } from 'react-apollo';
import { 
  ApolloClient, 
  InMemoryCache, 
} from 'apollo-client-preset';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import AsyncStorage from '@react-native-community/async-storage';

const httpLink = createHttpLink({
  uri: 'http://13.52.145.212:8080/graphql',
});
const authLink = setContext((_, { headers }) => {
 return AsyncStorage.getItem('token').then((data) => {
  let token = data;
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(httpLink),
  // link: new HttpLink({uri: 'http://13.52.145.212:8080/graphql'})
})

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1}}>
        <ApolloProvider client={client}>
          <AppContainer/>
        </ApolloProvider>
      </View>
    );
  }
}

